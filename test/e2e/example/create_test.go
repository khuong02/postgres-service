package example_test

import (
	"net/http"

	"gitlab.com/template-khuongdao/pkg/converttype"

	"gitlab.com/template-khuongdao/architecture/internal/example/payload"
	example "gitlab.com/template-khuongdao/architecture/internal/example/routers"
)

const TABLE_NAME = "examples"

func (suite *TestSuite) TestCreateExample() {
	suite.SetupTest()
	defer suite.truncateTable(TABLE_NAME)

	payload := payload.CreateExampleRequest{Name: converttype.String("test")}
	statusCode := http.StatusCreated

	r := example.Route{UseCase: suite.service.Example}
	w, req := suite.setupTestCreateExample(payload)

	suite.router.POST("/api/examples", r.Create)

	suite.router.ServeHTTP(w, req)
	suite.Equal(statusCode, w.Code)

	suite.truncateTable(TABLE_NAME)
}

func (suite *TestSuite) TestCreateExampleInvalidParams() {
	suite.SetupTest()
	defer suite.truncateTable(TABLE_NAME)

	payload := payload.CreateExampleRequest{Name: converttype.String(" ")}
	statusCode := http.StatusBadRequest

	r := example.Route{UseCase: suite.service.Example}
	w, req := suite.setupTestCreateExample(payload)

	suite.router.POST("/api/examples", r.Create)

	suite.router.ServeHTTP(w, req)
	suite.Equal(statusCode, w.Code)

	suite.truncateTable(TABLE_NAME)
}

func (suite *TestSuite) TestCreateExampleNotExitsParam() {
	suite.SetupTest()
	defer suite.truncateTable(TABLE_NAME)

	payload := payload.CreateExampleRequest{Name: nil}
	statusCode := http.StatusBadRequest

	r := example.Route{UseCase: suite.service.Example}
	w, req := suite.setupTestCreateExample(payload)

	suite.router.POST("/api/examples", r.Create)

	suite.router.ServeHTTP(w, req)
	suite.Equal(statusCode, w.Code)

	suite.truncateTable(TABLE_NAME)
}

func (suite *TestSuite) TestCreateExampleBadRequest() {
	suite.SetupTest()
	defer suite.truncateTable(TABLE_NAME)

	payload := map[string]interface{}{
		"param": "bad request",
	}
	statusCode := http.StatusBadRequest

	r := example.Route{UseCase: suite.service.Example}
	w, req := suite.setupTestCreateExample(payload)

	suite.router.POST("/api/examples", r.Create)

	suite.router.ServeHTTP(w, req)
	suite.Equal(statusCode, w.Code)

	suite.truncateTable(TABLE_NAME)
}
