package example_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"

	"gitlab.com/template-khuongdao/architecture/client/postgressql"
	"gitlab.com/template-khuongdao/architecture/internal/app"
	"gitlab.com/template-khuongdao/architecture/internal/migrations"
)

type TestSuite struct {
	suite.Suite
	*require.Assertions

	ctrl *gomock.Controller

	getDB func() *gorm.DB

	service *app.Service

	router *gin.Engine
}

func TestServiceExample(t *testing.T) {
	t.Parallel()
	suite.Run(t, new(TestSuite))
}

func (suite *TestSuite) SetupTest() {
	suite.Assertions = require.New(suite.T())

	suite.ctrl = gomock.NewController(suite.T())

	client := postgressql.GetClient

	suite.getDB = client
	suite.router = gin.Default()

	// migration
	migrations.Up(client())

	suite.service = app.New(client)
}

func (suite *TestSuite) TearDownTest() {
	suite.ctrl.Finish()
}

func (suite *TestSuite) truncateTable(tablename string) {
	suite.getDB().Exec(fmt.Sprintf("TRUNCATE TABLE %v", tablename))
}

func (suite *TestSuite) setupTestCreateExample(payload interface{}) (*httptest.ResponseRecorder, *http.Request) {
	b, _ := json.Marshal(payload)
	req := httptest.NewRequest(http.MethodPost, "/api/examples", bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	rec := httptest.NewRecorder()

	return rec, req
}
