package migrations

//nolint:goimports
import (
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	// Register using Golang migrate.
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/pkg/errors"
	"gitlab.com/template-khuongdao/pkg/mylogger"
	"gorm.io/gorm"

	"gitlab.com/template-khuongdao/architecture/config"
)

func Up(db *gorm.DB) {
	cfg := config.GetConfig()

	getDB, err := db.DB()
	if err != nil {
		mylogger.GetLogger().Fatal(err.Error())
	}

	driver, err := postgres.WithInstance(getDB, &postgres.Config{MigrationsTable: "migration"})
	if err != nil {
		mylogger.GetLogger().Fatal(err.Error())
	}

	m, err := migrate.NewWithDatabaseInstance(fmt.Sprintf("file://%v", cfg.Migration.Dir), cfg.Postgres.DB, driver)
	if err != nil {
		mylogger.GetLogger().Fatal(err.Error())
	}

	err = m.Up()
	if err != nil && !errors.Is(err, migrate.ErrNoChange) {
		mylogger.GetLogger().Fatal(err.Error())
	}

	mylogger.GetLogger().Info("Up done!")
}
