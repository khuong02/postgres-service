package payload

import (
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/template-khuongdao/architecture/utils/codeerror"
	"gitlab.com/template-khuongdao/architecture/utils/codetype"
)

type GetExampleRequest struct {
	ID *int64 `json:"id"`
}

type CreateExampleRequest struct {
	Name *string `json:"name"`
}

type GetExamplesRequest struct {
	codetype.Paginator
}

func (req *GetExampleRequest) Validate() error {
	if req.ID == nil || *req.ID < 0 {
		return codeerror.ErrFindByIDFailed(errors.New("Wrong ID"))
	}

	return nil
}

func (req *CreateExampleRequest) Validate() error {
	if req.Name == nil || strings.TrimSpace(*req.Name) == "" {
		return codeerror.ErrCreateBadRequest(errors.New("Invalid Params"))
	}

	return nil
}
