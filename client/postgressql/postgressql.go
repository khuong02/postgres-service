package postgressql

import (
	"fmt"
	"time"

	"gitlab.com/template-khuongdao/pkg/mylogger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/template-khuongdao/architecture/config"
)

var db *gorm.DB

func init() {
	var (
		err error
		cfg = config.GetConfig()
	)

	dns := fmt.Sprintf(
		"postgresql://%v:%v@%v:%v/%v?sslmode=disable",
		cfg.Postgres.User,
		cfg.Postgres.Pass,
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.DB,
	)

	db, err = gorm.Open(postgres.Open(dns), &gorm.Config{})
	if err != nil {
		mylogger.GetLogger().Info(err.Error())
	}

	if config.GetConfig().Stage.IsLocal() {
		db = db.Debug()
	}

	rawDB, _ := db.DB()
	// rawDB.SetConnMaxIdleTime(time.Hour)
	rawDB.SetMaxIdleConns(cfg.Postgres.DBMaxIdleConns)
	rawDB.SetMaxOpenConns(cfg.Postgres.DBMaxOpenConns)
	rawDB.SetConnMaxLifetime(time.Minute * 5)

	err = rawDB.Ping()
	if err != nil {
		mylogger.GetLogger().Fatal(err.Error())
	}

	mylogger.GetLogger().Info("Connected postgres db")
}

func GetClient() *gorm.DB {
	return db.Session(&gorm.Session{})
}

func Disconnect() {
	postgresDB, err := db.DB()
	if err != nil {
		mylogger.GetLogger().Fatal(err.Error())
	}

	if db != nil {
		_ = postgresDB.Close()
	}
}
