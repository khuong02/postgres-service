## Docker build
- docker build -t architecture . --build-arg SSH_PRIVATE_KEY="$(cat ~/.ssh/id_rsa)"

## Run Database Local
- make local-db

## Run App
- make run

## gci
- make gci

## Lint
- make lint

## Gen migrate
- make gen-migrate

## Host docker
- host.docker.internal

## Setup pkg
- export GOPRIVATE="gitlab.com/template-khuongdao"  
- git config --global url."git@gitlab.com:".insteadof="https://gitlab.com"

## Swagger
- http://localhost:9000/docs/index.html

## Install Mockgen and gomock
- go install github.com/golang/mock/mockgen@v1.6.0
- export PATH=$PATH:$(go env GOPATH)/bin
- go get github.com/golang/mock/gomock
- go get github.com/golang/mock/mockgen

## Mock
- mockgen -destination=`path-output` -package=`package-name` `package gen mock` `interface-name`
- Example:
  - path-output: `internal/example/mocks/mock_repository.go`
  - package-name: `mocks`
  - package gen mock: `gitlab.com/template-khuongdao/architecture/internal/example/repositories`
  - interface name: `ExampleQuery`