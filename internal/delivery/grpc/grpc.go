package grpc

import (
	pgExample "gitlab.com/template-khuongdao/grpc/example-service"

	"gitlab.com/template-khuongdao/architecture/internal/app"
)

type Service struct {
	pgExample.UnimplementedServiceServer
	Service *app.Service
}
