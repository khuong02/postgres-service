package test

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/template-khuongdao/pkg/converttype"
	"gitlab.com/template-khuongdao/pkg/myerror"
	"gorm.io/gorm"

	"gitlab.com/template-khuongdao/architecture/internal/example/dtos/example"
	"gitlab.com/template-khuongdao/architecture/internal/example/entities"
	"gitlab.com/template-khuongdao/architecture/internal/example/payload"
	"gitlab.com/template-khuongdao/architecture/utils/codeerror"
	"gitlab.com/template-khuongdao/architecture/utils/codetype"
)

func (suite *TestSuite) TestCreateExample() {
	var testCases = []struct {
		nameReq   *string
		err       error
		expectErr error
		isErr     bool
		isMock    bool
	}{
		{
			nameReq:   converttype.String("Test"),
			err:       nil,
			expectErr: nil,
			isErr:     false,
			isMock:    true,
		},
		{
			nameReq:   converttype.String("Test"),
			err:       errors.New("Name Is Unique"),
			expectErr: codeerror.ErrCreate(errors.New("Name Is Unique")),
			isErr:     true,
			isMock:    true,
		},
		{
			nameReq:   converttype.String(""),
			err:       errors.New("Invalid Params"),
			expectErr: codeerror.ErrCreateBadRequest(errors.New("Invalid Params")),
			isErr:     true,
			isMock:    false,
		},
		{
			err:       errors.New("Invalid Params"),
			expectErr: codeerror.ErrCreateBadRequest(errors.New("Invalid Params")),
			isErr:     true,
			isMock:    false,
		},
	}

	for _, testCase := range testCases {
		req := payload.CreateExampleRequest{
			Name: testCase.nameReq,
		}
		if testCase.isMock {
			suite.MockExampleQuery.EXPECT().Create(entities.Example{
				Name: *testCase.nameReq,
			}).Return(testCase.err).Times(1)
		}

		err := suite.useCases.NewExampleUseCase().Create(req)

		if testCase.isErr {
			suite.Error(err)

			myErr := err.(myerror.MyError)
			suite.Equal(testCase.expectErr.(myerror.MyError).ErrorCode, myErr.ErrorCode)
			suite.Equal(testCase.expectErr.(myerror.MyError).HTTPCode, myErr.HTTPCode)
		} else {
			suite.NoError(err)
		}
	}
}

func (suite *TestSuite) TestGetExampleByID() {
	var testCases = []struct {
		id        int64
		expect    *entities.Example
		err       error
		expectErr error
		isErr     bool
		isMock    bool
	}{
		{
			//Success
			id: 1,
			expect: &entities.Example{
				ID:        1,
				Name:      "Test",
				CreatedAt: time.Time{},
			},
			err:       nil,
			expectErr: nil,
			isErr:     false,
			isMock:    true,
		},
		{
			//Not Found
			id:        1,
			expect:    nil,
			err:       gorm.ErrRecordNotFound,
			expectErr: codeerror.ErrNotFound(gorm.ErrRecordNotFound),
			isErr:     true,
			isMock:    true,
		},
		{
			//Validate fail
			id:        -1,
			expect:    nil,
			err:       errors.New("Wrong ID"),
			expectErr: codeerror.ErrFindByIDFailed(errors.New("Wrong ID")),
			isErr:     true,
			isMock:    false,
		},
		{
			//Find Failed
			id:        1,
			expect:    nil,
			err:       errors.New("Find failed"),
			expectErr: codeerror.ErrFindByIDFailed(errors.New("Find failed")),
			isErr:     true,
			isMock:    true,
		},
	}

	for _, testCase := range testCases {
		req := payload.GetExampleRequest{
			ID: &testCase.id,
		}
		if testCase.isMock {
			suite.MockExampleQuery.EXPECT().GetByID(testCase.id).Return(testCase.expect, testCase.err).Times(1)
		}
		resp, err := suite.useCases.NewExampleUseCase().GetExampleByID(req)

		if testCase.isErr {
			suite.Error(err)
			suite.Nil(resp)

			myErr := err.(myerror.MyError)
			suite.Equal(testCase.expectErr.(myerror.MyError).ErrorCode, myErr.ErrorCode)
			suite.Equal(testCase.expectErr.(myerror.MyError).HTTPCode, myErr.HTTPCode)
		} else {
			expect := example.NewDTO(*testCase.expect)
			suite.NoError(err)
			suite.Equal(expect, resp)
		}
	}
}

func (suite *TestSuite) TestGetExamples() {
	var paginatorReq = codetype.Paginator{
		Page:  1,
		Limit: 2,
	}
	var testCases = []struct {
		req       payload.GetExamplesRequest
		expect    []entities.Example
		total     int64
		err       error
		expectErr error
		isErr     bool
		isMock    bool
	}{
		{
			//Success
			req: payload.GetExamplesRequest{
				paginatorReq,
			},
			expect: []entities.Example{
				{
					ID:        1,
					Name:      "Test",
					CreatedAt: time.Time{},
				},
				{
					ID:        2,
					Name:      "Test2",
					CreatedAt: time.Time{},
				},
			},
			total:     3,
			err:       nil,
			expectErr: nil,
			isErr:     false,
			isMock:    true,
		},
		{
			//Get Examples Failed
			req: payload.GetExamplesRequest{
				paginatorReq,
			},
			expect:    nil,
			total:     0,
			err:       errors.New("Get Examples Failed"),
			expectErr: codeerror.ErrGetExamples(errors.New("Get Examples Failed")),
			isErr:     true,
			isMock:    true,
		},
	}

	for _, testCase := range testCases {
		if testCase.isMock {
			paginator := testCase.req.Paginator
			paginator.Format()

			suite.MockExampleQuery.EXPECT().GetList(paginator).Return(testCase.expect, testCase.total, testCase.err).Times(1)
		}
		resp, err := suite.useCases.NewExampleUseCase().GetExamples(testCase.req)

		if testCase.isErr {
			suite.Error(err)
			suite.Nil(resp)

			myErr := err.(myerror.MyError)
			suite.Equal(testCase.expectErr.(myerror.MyError).ErrorCode, myErr.ErrorCode)
			suite.Equal(testCase.expectErr.(myerror.MyError).HTTPCode, myErr.HTTPCode)
		} else {
			dtos := make([]example.DTO, 0, len(testCase.expect))

			metadata := example.Metadata{
				Page:  int64(testCase.req.Page),
				Limit: int64(testCase.req.Limit),
				Total: testCase.total,
			}

			for idx := range testCase.expect {
				dtos = append(dtos, *example.NewDTO(testCase.expect[idx]))
			}

			expect := example.NewExamplesDTO(dtos, metadata)
			suite.NoError(err)
			suite.Equal(expect, resp)
		}
	}
}
