package config

import (
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
	"gitlab.com/template-khuongdao/pkg/mylogger"

	"gitlab.com/template-khuongdao/architecture/utils/codetype"
)

var config *Config

type Config struct {
	Port        string             `envconfig:"PORT"`
	ServiceHost string             `envconfig:"SERVICE_HOST"`
	Stage       codetype.StageType `envconfig:"STAGE"`

	Migration struct {
		Dir string `envconfig:"DIR_MIGRATION"`
	}

	Postgres struct {
		Host string `envconfig:"DB_HOST"`
		Port int    `envconfig:"DB_PORT"`
		DB   string `envconfig:"DB_NAME"`
		User string `envconfig:"DB_USER"`
		Pass string `envconfig:"DB_PASS"`

		DBMaxIdleConns int `envconfig:"DB_MAX_IDLE_CONNS"`
		DBMaxOpenConns int `envconfig:"DB_MAX_OPEN_CONNS"`
		CountRetryTx   int `envconfig:"DB_TX_RETRY_COUNT"`
	}
}

func init() {
	config = &Config{}

	_ = godotenv.Load()

	err := envconfig.Process("", config)
	if err != nil {
		err = errors.Wrap(err, "Failed to decode config env")
		mylogger.GetLogger().Fatal(err.Error())
	}
}

func GetConfig() *Config {
	return config
}
