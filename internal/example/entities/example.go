package entities

import "time"

type Example struct {
	ID        int64
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

func (entity Example) GetTableName() string {
	return "examples"
}
