package app

import (
	"gorm.io/gorm"

	ucExample "gitlab.com/template-khuongdao/architecture/internal/example/usecases"
)

type Service struct {
	Example ucExample.IExampleUseCases
}

func New(getDB func() *gorm.DB) *Service {
	return &Service{
		Example: ucExample.New(getDB),
	}
}
