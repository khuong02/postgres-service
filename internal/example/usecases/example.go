package usecases

import (
	"github.com/pkg/errors"
	"gitlab.com/template-khuongdao/architecture/internal/example/dtos/example"
	"gitlab.com/template-khuongdao/architecture/internal/example/entities"
	"gitlab.com/template-khuongdao/architecture/internal/example/payload"
	"gitlab.com/template-khuongdao/architecture/internal/example/repositories"
	"gitlab.com/template-khuongdao/architecture/utils/codeerror"
	"gorm.io/gorm"
)

type ExampleUseCase interface {
	Create(req payload.CreateExampleRequest) error
	GetExampleByID(req payload.GetExampleRequest) (*example.DTO, error)
	GetExamples(req payload.GetExamplesRequest) (*example.ExamplesDTO, error)
}

type exampleUseCase struct {
	repo repositories.ExampleQuery
}

func (uc *exampleUseCase) Create(req payload.CreateExampleRequest) error {
	if err := req.Validate(); err != nil {
		return err
	}

	example := entities.Example{
		Name: *req.Name,
	}

	if err := uc.repo.Create(example); err != nil {
		return codeerror.ErrCreate(err)
	}

	return nil
}

func (uc *exampleUseCase) GetExampleByID(req payload.GetExampleRequest) (*example.DTO, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	resp, err := uc.repo.GetByID(*req.ID)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, codeerror.ErrNotFound(err)
		}

		return nil, codeerror.ErrFindByIDFailed(err)
	}

	return example.NewDTO(*resp), nil
}

func (uc *exampleUseCase) GetExamples(req payload.GetExamplesRequest) (*example.ExamplesDTO, error) {
	dtos := make([]example.DTO, 0)

	req.Paginator.Format()

	examples, total, err := uc.repo.GetList(req.Paginator)
	if err != nil {
		return nil, codeerror.ErrGetExamples(err)
	}

	for idx := range examples {
		dtos = append(dtos, *example.NewDTO(examples[idx]))
	}

	return example.NewExamplesDTO(dtos, example.Metadata{
		Page:  int64(req.Page),
		Limit: int64(req.Limit),
		Total: total,
	}), nil
}
