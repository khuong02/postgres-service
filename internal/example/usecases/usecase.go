package usecases

import (
	"gitlab.com/template-khuongdao/architecture/internal/example/repositories"
	"gorm.io/gorm"
)

type IExampleUseCases interface {
	NewExampleUseCase() ExampleUseCase
}

type ExampleUseCases struct {
	ExampleRepo repositories.ExampleQuery
}

func New(getDB func() *gorm.DB) IExampleUseCases {
	return &ExampleUseCases{
		ExampleRepo: repositories.New(getDB).NewExample(),
	}
}

func (uc *ExampleUseCases) NewExampleUseCase() ExampleUseCase {
	return &exampleUseCase{
		repo: uc.ExampleRepo,
	}
}
