package example

import (
	pgExample "gitlab.com/template-khuongdao/grpc/example-service"

	"gitlab.com/template-khuongdao/architecture/internal/example/entities"
)

type DTO struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

func NewDTO(entity entities.Example) *DTO {
	return &DTO{
		ID:   entity.ID,
		Name: entity.Name,
	}
}

func (dto DTO) BindExampleDTOGrpc() *pgExample.GetExampleResponse {
	return &pgExample.GetExampleResponse{
		Id:   dto.ID,
		Name: dto.Name,
	}
}
