package codeerror

import (
	"net/http"

	"gitlab.com/template-khuongdao/pkg/myerror"
)

func ErrGetExamples(err error) myerror.MyError {
	return myerror.MyError{
		Raw:          err,
		HTTPCode:     http.StatusInternalServerError,
		ErrorCode:    "10500",
		ErrorMessage: "Get List Example Failed.",
	}
}

func ErrNotFound(err error) myerror.MyError {
	return myerror.MyError{
		Raw:          err,
		HTTPCode:     http.StatusNotFound,
		ErrorCode:    "10404",
		ErrorMessage: "Example Not Found.",
	}
}

func ErrFindByIDFailed(err error) myerror.MyError {
	return myerror.MyError{
		Raw:          err,
		HTTPCode:     http.StatusInternalServerError,
		ErrorCode:    "10500",
		ErrorMessage: "Example Find By ID Failed.",
	}
}

func ErrCreate(err error) myerror.MyError {
	return myerror.MyError{
		Raw:          err,
		HTTPCode:     http.StatusInternalServerError,
		ErrorCode:    "10500",
		ErrorMessage: "Create Example Failed.",
	}
}

func ErrCreateBadRequest(err error) myerror.MyError {
	return myerror.MyError{
		Raw:          err,
		HTTPCode:     http.StatusBadRequest,
		ErrorCode:    "10500",
		ErrorMessage: "Bad Request.",
	}
}
