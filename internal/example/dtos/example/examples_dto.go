package example

import pg "gitlab.com/template-khuongdao/grpc/example-service"

type Metadata struct {
	Page  int64 `json:"page"`
	Limit int64 `json:"limit"`
	Total int64 `json:"total"`
}

type ExamplesDTO struct {
	Data     []DTO    `json:"data"`
	Metadata Metadata `json:"metadata"`
}

func NewExamplesDTO(data []DTO, metadata Metadata) *ExamplesDTO {
	return &ExamplesDTO{
		Data:     data,
		Metadata: metadata,
	}
}

func (dtos ExamplesDTO) BindExamplesDTOGrpc() *pg.GetExamplesResponse {
	metadata := pg.GetExamplesResponse_Metadata{
		Page:  dtos.Metadata.Page,
		Limit: dtos.Metadata.Limit,
		Total: dtos.Metadata.Total,
	}

	resp := pg.GetExamplesResponse{
		ExamplesResponse: make([]*pg.GetExampleResponse, 0, len(dtos.Data)),
		Metadata:         &metadata,
	}

	for idx := range dtos.Data {
		resp.ExamplesResponse = append(resp.ExamplesResponse, dtos.Data[idx].BindExampleDTOGrpc())
	}

	return &resp
}
