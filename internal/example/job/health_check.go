package job

import (
	"github.com/robfig/cron/v3"
	"gitlab.com/template-khuongdao/pkg/mylogger"
)

type healthChecks struct{}

func NewHealthChecks() IJob {
	return healthChecks{}
}

func (h healthChecks) callHealthCheck() {
	mylogger.GetLogger().Info("run job")
}

func (h healthChecks) Run() {
	c := cron.New()

	_, err := c.AddFunc("* * * * 1", func() { h.callHealthCheck() })
	if err != nil {
		mylogger.GetLogger().Fatal("failed to schedule health check short period")
	}

	c.Start()
}
