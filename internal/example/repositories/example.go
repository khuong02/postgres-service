//go:generate mockgen -source=example.go -destination=../mocks/mock_repository.go -package=mocks ExampleQuery

package repositories

import (
	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	"gitlab.com/template-khuongdao/architecture/internal/example/entities"
	"gitlab.com/template-khuongdao/architecture/utils/codetype"
)

type ExampleQuery interface {
	GetByID(id int64) (*entities.Example, error)
	Create(example entities.Example) error
	CreateOrUpdate(example entities.Example) error
	CreateOrUpdateList(example []entities.Example) error
	Update(example entities.Example) error
	UpdateList(examples []entities.Example) error
	Delete(example entities.Example, unscoped bool) error
	DeleteList(examples []entities.Example, unscoped bool) error
	GetList(
		paginator codetype.Paginator,
	) ([]entities.Example, int64, error)
}

type exampleRepo struct {
	getDB func() *gorm.DB
}

func (pg exampleRepo) GetByID(id int64) (*entities.Example, error) {
	var example entities.Example

	err := pg.getDB().Table(example.GetTableName()).Where("id = ?", id).First(&example).Error
	if err != nil {
		return nil, err
	}

	return &example, nil
}

func (pg exampleRepo) Create(example entities.Example) error {
	return pg.getDB().Table(example.GetTableName()).Create(&example).Error
}

func (pg exampleRepo) CreateList(examples []entities.Example) error {
	var example entities.Example

	return pg.getDB().Table(example.GetTableName()).Create(&examples).Error
}

func (pg exampleRepo) CreateOrUpdate(example entities.Example) error {
	return pg.getDB().
		Table(example.GetTableName()).
		Clauses(clause.OnConflict{
			DoUpdates: clause.AssignmentColumns([]string{"id"}),
		}).
		Create(&example).
		Error
}

func (pg exampleRepo) CreateOrUpdateList(examples []entities.Example) error {
	var example entities.Example

	return pg.getDB().
		Table(example.GetTableName()).
		Clauses(clause.OnConflict{
			DoUpdates: clause.AssignmentColumns([]string{"id"}),
		}).
		Create(&examples).
		Error
}

func (pg exampleRepo) Update(example entities.Example) error {
	return pg.getDB().Table(example.GetTableName()).Save(example).Error
}

func (pg exampleRepo) UpdateList(examples []entities.Example) error {
	var example entities.Example

	return pg.getDB().Table(example.GetTableName()).Save(examples).Error
}

func (pg exampleRepo) Delete(example entities.Example, unscoped bool) error {
	db := pg.getDB()

	if unscoped {
		db = db.Unscoped()
	}

	return db.Delete(&example).Error
}

func (pg exampleRepo) DeleteList(examples []entities.Example, unscoped bool) error {
	db := pg.getDB()

	if unscoped {
		db = db.Unscoped()
	}

	return db.Delete(examples).Error
}

func (pg exampleRepo) GetList(paginator codetype.Paginator) ([]entities.Example, int64, error) {
	var (
		example  entities.Example
		examples []entities.Example
		total    int64
	)

	err := pg.getDB().
		Table(example.GetTableName()).
		Offset((paginator.Page - 1) * paginator.Limit).
		Limit(paginator.Limit).
		Scan(&examples).
		Error
	if err != nil {
		return nil, 0, err
	}

	err = pg.getDB().
		Table(example.GetTableName()).
		Count(&total).
		Error
	if err != nil {
		return nil, 0, err
	}

	if paginator.Limit == -1 {
		total = int64(len(examples))
	}

	return examples, total, nil
}
