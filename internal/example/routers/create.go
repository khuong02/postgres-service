package routers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/template-khuongdao/pkg/helper"
	"gitlab.com/template-khuongdao/pkg/myerror"

	"gitlab.com/template-khuongdao/architecture/internal/example/payload"
)

// Create Example
// @Summary Create example
// @Description create a example
// @Tags Example
// @Accept  json
// @Produce json
// @Param req body payload.CreateExampleRequest true "Example info"
// @Success 200
// @Router /examples [post] .
func (r Route) Create(c *gin.Context) {
	var req payload.CreateExampleRequest

	if err := c.BindJSON(&req); err != nil {
		helper.Response.Error(c, myerror.NewMyError(err, http.StatusBadRequest, "100400", "Bad request"))

		return
	}

	err := r.UseCase.NewExampleUseCase().Create(req)
	if err != nil {
		helper.Response.Error(c, err.(myerror.MyError))

		return
	}

	helper.Response.Success(c, http.StatusCreated, nil)
}
