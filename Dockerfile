FROM golang:1.18-alpine as builder

ARG SSH_PRIVATE_KEY
RUN echo $SSH_PRIVATE_KEY
RUN apk add --no-cache ca-certificates \
    dpkg \
    gcc \
    git \
    musl-dev \
    openssh

ENV GO111MODULE=on

RUN mkdir -p ~/.ssh && \
    umask 0077 && \
    echo "${SSH_PRIVATE_KEY}" > ~/.ssh/id_rsa && \
    echo "StrictHostKeyChecking no " > ~/.ssh/config && \
    git config --global url.ssh://git@gitlab.com/.insteadOf https://gitlab.com/

ENV APPDIR /app
WORKDIR $APPDIR

COPY go.mod .
COPY go.sum .

RUN go mod download
RUN ls
COPY .. .

EXPOSE 9000

RUN CGO_ENABLED=0 GOOS=linux go build -o server ./cmd/server
RUN CGO_ENABLED=0 GOOS=linux go build -o job ./cmd/job

FROM alpine:latest

WORKDIR /app

RUN apk --no-cache add ca-certificates tzdata

COPY --from=builder /app/internal/migrations ./internal/migrations/
COPY --from=builder /app/server ./
COPY --from=builder /app/job ./

CMD ["./server"]