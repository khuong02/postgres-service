package routers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/template-khuongdao/architecture/internal/example/usecases"
)

type Route struct {
	UseCase usecases.IExampleUseCases
}

func Init(group *gin.RouterGroup, useCase usecases.IExampleUseCases) {
	r := &Route{
		UseCase: useCase,
	}

	group.GET("/", r.Get)
	group.GET("/:id", r.GetByID)

	group.POST("/", r.Create)
}
