package test

import (
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/template-khuongdao/architecture/internal/example/mocks"
	"gitlab.com/template-khuongdao/architecture/internal/example/usecases"
)

type TestSuite struct {
	suite.Suite
	*require.Assertions

	ctrl *gomock.Controller

	MockExampleQuery *mocks.MockExampleQuery

	useCases usecases.IExampleUseCases
}

func TestUseCaseExample(t *testing.T) {
	t.Parallel()
	suite.Run(t, new(TestSuite))
}

func (suite *TestSuite) SetupTest() {
	suite.Assertions = require.New(suite.T())

	suite.ctrl = gomock.NewController(suite.T())
	suite.MockExampleQuery = mocks.NewMockExampleQuery(suite.ctrl)

	suite.useCases = &usecases.ExampleUseCases{
		ExampleRepo: suite.MockExampleQuery,
	}
}

func (suite *TestSuite) TearDownTest() {
	suite.ctrl.Finish()
}
