build:
	go build -ldflags "-s -w" -o ./tmp/server ./cmd/server/main.go

run:
	air -c .air.conf

unit-test:
	@mkdir coverage || true
	@godotenv -f ./.env.test go test -race -v -coverprofile=coverage/coverage.txt.tmp -count=1 -cover -coverpkg ./...  ./...
	@cat coverage/coverage.txt.tmp | grep -v "mock_" > coverage/coverage.txt
	@go tool cover -func=coverage/coverage.txt
	@go tool cover -html=coverage/coverage.txt -o coverage/index.html

proto:
	@protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative api/proto/example.proto

local-db:
	@docker-compose down
	@docker-compose up -d

gen-migrate:
	@$(eval NAME := $(shell read -p "Enter your file name: " v && echo $$v))
	migrate create -ext sql -dir ./internal/migrations ${NAME}

swagger:
	@@hash swag 2>/dev/null || GO111MODULE=off go get -u github.com/swaggo/swag/cmd/swag
	swag init -g cmd/server/main.go --parseDependency --parseInternal --parseDepth 2

gci:
	@gci write -s standard -s default -s "Prefix(gitlab.com/template-khuongdao/architecture)" .

lint:
	@hash golangci-lint 2>/dev/null || curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.48.0
	@GO111MODULE=on CGO_ENABLED=0 golangci-lint run

docker-build:
	@docker build -t khuong02/architecture . --build-arg SSH_PRIVATE_KEY="$$(cat ~/.ssh/id_rsa)"

lint-consistent:
	@hash go-consistent 2>/dev/null || GO111MODULE=off go get -v github.com/quasilyte/go-consistent
	@go-consistent ./...

sec:
	@curl -sfL https://raw.githubusercontent.com/securego/gosec/master/install.sh | sh -s -- -b $(GOPATH)/bin latest
	@gosec ./...

mock:
	@go generate -v ./...

clean-cache-test:
	@go clean -testcache

open-coverage:
	@open coverage/index.html
