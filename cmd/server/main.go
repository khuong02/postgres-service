package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/soheilhy/cmux"
	pgExample "gitlab.com/template-khuongdao/grpc/example-service"
	"gitlab.com/template-khuongdao/pkg/mylogger"
	"google.golang.org/grpc"
	"gorm.io/gorm"

	"gitlab.com/template-khuongdao/architecture/client/postgressql"
	"gitlab.com/template-khuongdao/architecture/config"
	"gitlab.com/template-khuongdao/architecture/docs"
	"gitlab.com/template-khuongdao/architecture/internal/app"
	serviceGRPC "gitlab.com/template-khuongdao/architecture/internal/delivery/grpc"
	serviceHttp "gitlab.com/template-khuongdao/architecture/internal/delivery/http"
	"gitlab.com/template-khuongdao/architecture/internal/example/job"
	"gitlab.com/template-khuongdao/architecture/internal/migrations"
)

// @title Example API
// @version 1.0

// @BasePath /api
// @schemes http https

// @securityDefinitions.apikey AuthToken
// @in header
// @name Authorization

// @description Transaction API.
func main() {
	taskPtr := flag.String("task", "server", "server")

	flag.Parse()

	defer postgressql.Disconnect()

	client := postgressql.GetClient
	service := app.New(client)

	switch *taskPtr {
	case "server":
		executeServer(service, client)
	default:
		executeServer(service, client)
	}
}

func executeServer(
	service *app.Service,
	client func() *gorm.DB,
) {
	cfg := config.GetConfig()

	// migration
	migrations.Up(client())

	// cronjob
	job.New().Run()

	// swagger
	docs.SwaggerInfo.Host = cfg.ServiceHost

	l, err := net.Listen("tcp", ":"+cfg.Port)
	if err != nil {
		mylogger.GetLogger().Fatal(err.Error())
	}

	m := cmux.New(l)
	grpcL := m.MatchWithWriters(cmux.HTTP2MatchHeaderFieldSendSettings("content-type", "application/grpc"))
	httpL := m.Match(cmux.HTTP1Fast())
	errs := make(chan error)

	// http
	{
		go func() {
			httpS := &http.Server{
				Addr:              fmt.Sprintf(":%v", config.GetConfig().Port),
				Handler:           serviceHttp.New(service).NewHTTPHandler(),
				ReadHeaderTimeout: 3 * time.Second,
			}
			mylogger.GetLogger().Info("Server running on port: " + cfg.Port)

			errs <- httpS.Serve(httpL)
		}()
	}

	// gRPC
	{
		s := grpc.NewServer()

		grpcServer := &serviceGRPC.Service{
			Service: service,
		}

		pgExample.RegisterServiceServer(s, grpcServer)

		go func() {
			errs <- s.Serve(grpcL)
		}()
	}

	go func() {
		errs <- m.Serve()
	}()

	err = <-errs
	if err != nil {
		mylogger.GetLogger().Fatal(err.Error())
	}
}
