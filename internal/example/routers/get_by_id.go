package routers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/template-khuongdao/pkg/converttype"
	"gitlab.com/template-khuongdao/pkg/helper"
	"gitlab.com/template-khuongdao/pkg/myerror"

	"gitlab.com/template-khuongdao/architecture/internal/example/dtos/example"
	"gitlab.com/template-khuongdao/architecture/internal/example/payload"
)

// GetByID Example
// @Summary GetByID example
// @Description GetByID example
// @Tags Example
// @Accept  json
// @Produce json
// @Param req body payload.GetExampleRequest true "Example info"
// @Success 200 {object} example.DTO
// @Router /examples/{id} [get] .
func (r Route) GetByID(c *gin.Context) {
	var (
		idStr = c.Param("id")
		req   payload.GetExampleRequest
		resp  *example.DTO
	)

	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		helper.Response.Error(c, err.(myerror.MyError))

		return
	}

	req.ID = converttype.Int64(id)

	resp, err = r.UseCase.NewExampleUseCase().GetExampleByID(req)
	if err != nil {
		helper.Response.Error(c, err.(myerror.MyError))

		return
	}

	helper.Response.Success(c, http.StatusOK, resp)
}
