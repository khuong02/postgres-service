package repositories

import (
	"gorm.io/gorm"
)

type Repository interface {
	NewExample() ExampleQuery
}

type repository struct {
	getDB func() *gorm.DB
}

func New(getDB func() *gorm.DB) Repository {
	return &repository{
		getDB: getDB,
	}
}

func (repo *repository) NewExample() ExampleQuery {
	return &exampleRepo{
		getDB: repo.getDB,
	}
}
