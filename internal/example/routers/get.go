package routers

import (
	"gitlab.com/template-khuongdao/architecture/internal/example/dtos/example"
	"gitlab.com/template-khuongdao/architecture/internal/example/payload"
	"gitlab.com/template-khuongdao/pkg/helper"
	"gitlab.com/template-khuongdao/pkg/myerror"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetList Example
// @Summary GetList example
// @Description getList example
// @Tags Example
// @Accept  json
// @Produce json
// @Param req body payload.GetExamplesRequest true "Example info"
// @Success 200 {object} example.ExamplesDTO
// @Router /examples [get] .
func (r Route) Get(c *gin.Context) {
	var (
		req  payload.GetExamplesRequest
		resp *example.ExamplesDTO
	)

	if err := c.Bind(&req); err != nil {
		helper.Response.Error(c, err.(myerror.MyError))

		return
	}

	resp, err := r.UseCase.NewExampleUseCase().GetExamples(req)
	if err != nil {
		helper.Response.Error(c, err.(myerror.MyError))

		return
	}

	helper.Response.Success(c, http.StatusOK, resp)
}
