package grpc

import (
	"context"
	"gitlab.com/template-khuongdao/architecture/internal/example/payload"

	pg "gitlab.com/template-khuongdao/grpc/example-service"
	"gitlab.com/template-khuongdao/pkg/converttype"

	"gitlab.com/template-khuongdao/architecture/utils/codetype"
)

func (s Service) GetExampleByID(ctx context.Context, reqGrpc *pg.GetByIDRequest) (*pg.GetExampleResponse, error) {
	req := payload.GetExampleRequest{ID: converttype.Int64(reqGrpc.Id)}

	resp, err := s.Service.Example.NewExampleUseCase().GetExampleByID(req)
	if err != nil {
		return nil, err
	}

	return resp.BindExampleDTOGrpc(), nil
}

func (s Service) GetExamples(ctx context.Context, reqGrpc *pg.GetExamplesRequest) (*pg.GetExamplesResponse, error) {
	paginator := codetype.Paginator{
		Page:  int(reqGrpc.Page),
		Limit: int(reqGrpc.Limit),
	}

	req := payload.GetExamplesRequest{
		Paginator: paginator,
	}

	resp, err := s.Service.Example.NewExampleUseCase().GetExamples(req)
	if err != nil {
		return nil, err
	}

	return resp.BindExamplesDTOGrpc(), nil
}

func (s Service) CreateExample(
	ctx context.Context,
	reqGrpc *pg.CreateExampleRequest,
) (*pg.CreateExampleResponse, error) {
	req := payload.CreateExampleRequest{Name: &reqGrpc.Name}

	err := s.Service.Example.NewExampleUseCase().Create(req)
	if err != nil {
		return nil, err
	}

	return &pg.CreateExampleResponse{
		Status:  200,
		Info:    "Create successfully!",
		Message: "Create example successfully!",
	}, nil
}
