package http

import (
	"fmt"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"gitlab.com/template-khuongdao/architecture/config"
	"gitlab.com/template-khuongdao/architecture/internal/app"
	example "gitlab.com/template-khuongdao/architecture/internal/example/routers"
)

type ServiceHTTP struct {
	service *app.Service
}

func New(service *app.Service) *ServiceHTTP {
	return &ServiceHTTP{
		service: service,
	}
}

func (h *ServiceHTTP) NewHTTPHandler() *gin.Engine {
	r := gin.New()

	r.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		// your custom format
		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))
	r.Use(cors.Default())
	r.Use(gin.Recovery())

	// API docs
	if !config.GetConfig().Stage.IsProd() {
		r.GET("/docs/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
	}

	example.Init(r.Group("/api/examples"), h.service.Example)

	return r
}
